import os
import sys
from setuptools import setup, find_packages

PACKAGE_NAME = 'common_primitives'
MINIMUM_PYTHON_VERSION = 3, 6


def check_python_version():
    """Exit when the Python version is too low."""
    if sys.version_info < MINIMUM_PYTHON_VERSION:
        sys.exit("Python {}.{}+ is required.".format(*MINIMUM_PYTHON_VERSION))


def read_package_variable(key):
    """Read the value of a variable from the package without importing."""
    module_path = os.path.join(PACKAGE_NAME, '__init__.py')
    with open(module_path) as module:
        for line in module:
            parts = line.strip().split(' ')
            if parts and parts[0] == key:
                return parts[-1].strip("'")
    assert False, "'{0}' not found in '{1}'".format(key, module_path)


check_python_version()
version = read_package_variable('__version__')

setup(
    name=PACKAGE_NAME,
    version=version,
    description='D3M common primitives',
    author=read_package_variable('__author__'),
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    install_requires=[
        'd3m',
        'scikit-learn',
        'scikit-image',
        'numpy',
        'pymc3',
        'torch==0.3.1',
        'torchvision',
        'theano==1.0.1',
    ],
    entry_points = {
        'd3m.primitives': [
            'common_primitives.BayesianLogisticRegression = common_primitives.logistic_regression:BayesianLogisticRegression',
            'common_primitives.ConvolutionalNeuralNet = common_primitives.convolutional_neural_net:ConvolutionalNeuralNet',
            'common_primitives.DiagonalMVN = common_primitives.diagonal_mvn:DiagonalMVN',
            'common_primitives.FeedForwardNeuralNet = common_primitives.feed_forward_neural_net:FeedForwardNeuralNet',
            #'common_primitives.ImageReader = common_primitives.image_reader:ImageReader',
            'common_primitives.KMeans = common_primitives.k_means:KMeans',
            'common_primitives.LinearRegression = common_primitives.linear_regression:LinearRegression',
            'common_primitives.Loss = common_primitives.loss:Loss',
            #'common_primitives.PCA = common_primitives.pca:PCA',
            'common_primitives.RandomForestClassifier = common_primitives.random_forest:RandomForestClassifier',
            'data.ExtractColumnsBySemanticTypes = common_primitives.extract_columns_semantic_types:ExtractColumnsBySemanticTypesPrimitive',
            'data.ExtractColumnsByDtype = common_primitives.extract_columns_dtype:ExtractColumnsByDtypePrimitive',
            'data.Concat = common_primitives.concatenate:ConcatPrimitive',
            'data.CastToType = common_primitives.cast_to_type:CastToTypePrimitive',
            #'data.ColumnParser = common_primitives.column_parser:ColumnParserPrimitive',
            #'data.ConstructPredictions = common_primitives.construct_predictions:ConstructPredictionsPrimitive',
            #'datasets.Denormalize = common_primitives.denormalize:DenormalizePrimitive',
            'datasets.DatasetToDataFrame = common_primitives.dataset_to_dataframe:DatasetToDataFramePrimitive',
            'labels.OneHotMaker = common_primitives.one_hot_maker:OneHotMakerPrimitive',
            'datasets.UpdateSemanticTypes = common_primitives.update_semantic_types:UpdateSemanticTypesPrimitive',
            'datasets.RemoveColumns = common_primitives.remove_columns:RemoveColumnsPrimitive'
        ],
    },
    dependency_links=[
        'https://download.pytorch.org/whl/cpu/torch-0.3.1-cp36-cp36m-linux_x86_64.whl',
    ],
    url='https://gitlab.com/datadrivendiscovery/common-primitives',
)
