# How to publish primitive annotations

As contributors add or update their primitives they might want to publish
primitive annotations for added primitives. When doing this it is important
to republish also all other primitive annotations already published from this
package. This is because only one version of the package can be installed at
a time and all primitive annotations have to point to the same package in
their `installation` metadata.

Steps to publish primitive annotations:
* Update `HISTORY.md` for `vNEXT` release with information about primitives
  added or updated. If there was no package release since they were updated last,
  do not duplicate entries but just update any existing entries for those primitives
  instead, so that once released it is clear what has changed in a release as a whole.
* Make sure tests for primitives being published (primitives added, updated,
  and primitives previously published which should be now republished) pass.
* Update `setup.py` `entry_points` and add new primitives. Leave active
  only those entries for primitives being (re)published and comment out all others.
  If this is the first time primitives are published after a release of a new `d3m`
  core package, leave active only those which were updated to work with
  the new `d3m` core package. Leave to others to update, verify, and publish
  other common primitives.
* Update the list of primitives to generate primitive annotations for in
  `add.sh` script. Make sure the list matches entry points. Those and only those
  primitives which are active in entry points should be listed in `add.sh`.
  Do not remove items of the list, but comment them out.
* In `primitives_repo` prepare a branch of the up-to-date `master` branch
  to add/update primitive annotations.
* Run `add.sh` in root of this package, which will add primitive annotations
  to `primitives_repo`.
* Verify changes in the `primitives_repo`, add and commit files to git.
* Publish a branch in `primitives_repo` and make a merge request.

# How to release a new version

A new version is always released from `master` branch against a stable release
of `d3m` core package. A new version should be released when there are major
changes to the package (many new primitives added, larger breaking changes).
Sync up with other developers of the repo to suggest a release, or do a release.

* On `master` branch:
  * Make sure `HISTORY.md` file is updates with all changes since the last release.
  * Change a version in `common_primitives/__init__.py` to the to-be-released version, without `v` prefix.
  * Change `vNEXT` in `HISTORY.md` to the to-be-released version, with `v` prefix.
  * Commit with message `Bumping version for release.`
  * `git push`
  * Wait for CI to run tests successfully.
  * Tag with version prefixed with `v`, e.g., for version `0.2.0`: `git tag v0.2.0`
  * `git push` & `git push --tags`
  * Change a version in `common_primitives/__init__.py` back to `devel` string.
  * Add a new empty `vNEXT` version on top of `HISTORY.md`.
  * Commit with message `Version bump for development.`
  * `git push`  
* On `devel` branch:
  * Merge `devel` into `master` branch: `git merge devel`
  * Update the branch according to the section below.
  * `git push`

# How to update `master` branch after a release of new `d3m` core package

Hopefully, `devel` branch already contains code which works against the released
`d3m` core package. So merge `devel` branch into `master` branch and update
files according to the following section.

# Keeping `master` and `devel` branches in sync

Because `master` and `devel` branches mostly contain the same code,
just made against different version of `d3m` core package, it is common
to merge branches into each other as needed to keep them in sync.
When doing so, the following are files which are specific to branches:

* `.gitlab-ci.yml` has a `DEPENDENCY_REF` environment variable which
  has to point to `master` on `master` branch of this repository,
  and `devel` on `devel` branch of this repository.
