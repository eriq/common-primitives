import json
import math
import os
import unittest

from d3m import container, utils
from d3m.metadata import base as metadata_base

from common_primitives import dataset_to_dataframe, column_parser


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=utils.MetadataJsonEncoder))


class ColumnParserPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = primitive.produce(inputs=dataset)

        dataframe = call_metadata.value

        hyperparams_class = column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults())

        call_metadata = primitive.produce(inputs=dataframe)

        dataframe = call_metadata.value

        first_row = list(dataframe.itertuples(index=False, name=None))[0]

        self.assertEqual(first_row, (0, 5.1, 3.5, 1.4, 0.2, 6241605690342144121))

        self.assertEqual([type(o) for o in first_row], [int, float, float, float, float, int])

        self.assertIs(dataframe, dataframe.metadata.for_value)

        self._test_metadata(dataframe.metadata)

    def test_can_accept(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        dataset = container.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        inputs_metadata = dataset.metadata.set_for_value(None)

        dataframe_metadata = dataset_to_dataframe.DatasetToDataFramePrimitive.can_accept(method_name='produce', arguments={'inputs': inputs_metadata})

        self.assertTrue(dataframe_metadata)

        outputs_metadata = column_parser.ColumnParserPrimitive.can_accept(method_name='produce', arguments={'inputs': dataframe_metadata})

        self.assertTrue(outputs_metadata)

        self.assertIsNone(outputs_metadata.for_value)

        self._test_metadata(outputs_metadata)

    def _test_metadata(self, metadata):
        self.maxDiff = None

        self.assertEqual(convert_metadata(metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS,))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'int',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        for i in range(1, 5):
            self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'structural_type': 'float',
                'semantic_types': [
                    'http://schema.org/Float',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            })

        self.assertEqual(convert_metadata(metadata.query((metadata_base.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'structural_type': 'int',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

    def test_integer(self):
        hyperparams_class = column_parser.ColumnParserPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']

        primitive = column_parser.ColumnParserPrimitive(hyperparams=hyperparams_class.defaults())

        dataframe = container.DataFrame({'a': ['1.0', '2.0', '3.0']}, {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': container.DataFrame,
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 3,
            }
        })

        dataframe.metadata = dataframe.metadata.update((metadata_base.ALL_ELEMENTS,), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 1,
            }
        })

        dataframe.metadata = dataframe.metadata.update((metadata_base.ALL_ELEMENTS, 0), {
            'name': 'test',
            'structural_type': str,
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        call_metadata = primitive.produce(inputs=dataframe)

        parsed_dataframe = call_metadata.value

        self.assertEqual(convert_metadata(parsed_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'test',
            'structural_type': 'int',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(list(parsed_dataframe.iloc[:, 0]), [1, 2, 3])

        dataframe.iloc[2, 0] = '3.1'

        call_metadata = primitive.produce(inputs=dataframe)

        parsed_dataframe = call_metadata.value

        self.assertEqual(convert_metadata(parsed_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'test',
            'structural_type': 'int',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(list(parsed_dataframe.iloc[:, 0]), [1, 2, 3])

        dataframe.iloc[2, 0] = 'aaa'

        with self.assertRaisesRegex(ValueError, 'Not all values in a column can be parsed into integers, but only integers were expected'):
            primitive.produce(inputs=dataframe)

        dataframe.metadata = dataframe.metadata.update((metadata_base.ALL_ELEMENTS, 0), {
            'name': 'test',
            'structural_type': str,
            'semantic_types': [
                'http://schema.org/Integer',
            ],
        })

        call_metadata = primitive.produce(inputs=dataframe)

        parsed_dataframe = call_metadata.value

        self.assertEqual(convert_metadata(parsed_dataframe.metadata.query((metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'test',
            'structural_type': 'float',
            'semantic_types': [
                'http://schema.org/Integer',
            ],
        })

        self.assertEqual(list(parsed_dataframe.iloc[0:2, 0]), [1.0, 2.0])
        self.assertTrue(math.isnan(parsed_dataframe.iloc[2, 0]))


if __name__ == '__main__':
    unittest.main()
