import json
import math
import os
import unittest
import numpy as np
from d3m import container

from common_primitives import extract_columns_dtype


class ConcatPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):

        test_data = {'col1': [1.0, 2.0, 3.0], 'col2':[1, 2 ,3]}
        dataframe_original = container.DataFrame(data=test_data)
        dataframe_original['col2'] = dataframe_original.col2.astype(np.uint64)

        result_data = {'col2':[1, 2 ,3]}
        dataframe_result = container.DataFrame(data=result_data)
        dataframe_result['col2'] = dataframe_result.col2.astype(np.uint64)

        hyperparams_class = extract_columns_dtype.ExtractColumnsByDtypePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        primitive = extract_columns_dtype.ExtractColumnsByDtypePrimitive(hyperparams=hyperparams_class.defaults())

        call_result = primitive.produce(inputs=dataframe_original)

        dataframe_filtered = call_result.value

        self.assertEqual(dataframe_filtered['col2'].dtypes, dataframe_result['col2'].dtypes)
        self.assertEqual(dataframe_filtered['col2'].values.tolist(), result_data['col2'])


if __name__ == '__main__':
    unittest.main()
