# import os
import json
import unittest

from common_primitives import dataset_to_dataframe, remove_columns
from d3m import container, utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams

import utils as test_utils


class RemoveColumnsPrimitiveTestCase(unittest.TestCase):
    def test_basic(self):
        # load the iris dataset
        dataset = test_utils.load_iris_metadata()

        remove_columns_hyperparams_class = remove_columns.RemoveColumnsPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        hp = remove_columns_hyperparams_class({
            'columns': ('sepalLength', 'petalLength'),
            'resource_id': '0'
        })
        remove_columns_primitive = remove_columns.RemoveColumnsPrimitive(hyperparams=hp)

        dataset = remove_columns_primitive.produce(inputs=dataset).value

        expected = ['d3mIndex', 'sepalWidth', 'petalWidth', 'species']

        # validate metadata
        num_columns = dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS,))['dimension']['length']
        self.assertEqual(len(expected), num_columns)

        result_metadata = [dataset.metadata.query(('0', metadata_base.ALL_ELEMENTS, i))['name'] for i in range(num_columns)]
        self.assertListEqual(result_metadata, expected)

        # validate dataframe
        dataframe = list(dataset.values())[0]
        self.assertListEqual(list(dataframe.columns.values), expected)


if __name__ == '__main__':
    unittest.main()
