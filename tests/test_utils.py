import unittest

from d3m.metadata import base

from common_primitives import utils


class TestUtils(unittest.TestCase):
    def test_copy_elements_metadata(self):
        metadata = base.Metadata()

        metadata = metadata.update((), {'level0': 'foobar0'})

        metadata = metadata.update(('level1',), {'level1': 'foobar1'})

        metadata = metadata.update((base.ALL_ELEMENTS,), {'level1a': 'foobar1a', 'level1b': 'foobar1b'})

        metadata = metadata.update(('level1',), {'level1b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2'), {'level2': 'foobar2'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS), {'level2a': 'foobar2a', 'level2b': 'foobar2b'})

        metadata = metadata.update(('level1', 'level2'), {'level2b': base.NO_VALUE})

        metadata = metadata.update(('level1', 'level2', 'level3'), {'level3': 'foobar3'})

        metadata = metadata.update((base.ALL_ELEMENTS, base.ALL_ELEMENTS, 'level3'), {'level3a': 'foobar3a'})

        metadata = metadata.update(('level1', 'level2', 'level3.1'), {'level3.1': 'foobar3.1'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4'), {'level4': 'foobar4'})

        metadata = metadata.update(('level1', 'level2', 'level3', 'level4.1'), {'level4.1': 'foobar4.1'})

        self.assertTrue(metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0': 'foobar0'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level1a': 'foobar1a', 'level1b': 'foobar1b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', '__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level1'],
            'metadata': {'level1': 'foobar1', 'level1b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level1', 'level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level1', 'level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level1', 'level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(metadata.query(('level1', 'level2')), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, ('level1',), (), target_metadata)

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['__ALL_ELEMENTS__'],
            'metadata': {'level2a': 'foobar2a', 'level2b': 'foobar2b'},
        }, {
            'selector': ['__ALL_ELEMENTS__', 'level3'],
            'metadata': {'level3a': 'foobar3a'},
        }, {
            'selector': ['level2'],
            'metadata': {'level2': 'foobar2', 'level2b': '__NO_VALUE__'},
        }, {
            'selector': ['level2', 'level3'],
            'metadata': {'level3': 'foobar3'},
        }, {
            'selector': ['level2', 'level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level2', 'level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level2', 'level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])

        self.assertEqual(target_metadata.query(('level2',)), {
            'level2a': 'foobar2a',
            'level2': 'foobar2',
        })

        target_metadata = base.Metadata()

        target_metadata = target_metadata.update((), {'level0z': 'foobar0z'})

        target_metadata = utils.copy_elements_metadata(metadata, ('level1', 'level2'), (), target_metadata)

        self.assertEqual(target_metadata.to_json_structure(), [{
            'selector': [],
            'metadata': {'level0z': 'foobar0z'},
        }, {
            'selector': ['level3'],
            'metadata': {'level3': 'foobar3', 'level3a': 'foobar3a'},
        }, {
            'selector': ['level3', 'level4'],
            'metadata': {'level4': 'foobar4'},
        }, {
            'selector': ['level3', 'level4.1'],
            'metadata': {'level4.1': 'foobar4.1'},
        }, {
            'selector': ['level3.1'],
            'metadata': {'level3.1': 'foobar3.1'},
        }])


if __name__ == '__main__':
    unittest.main()
