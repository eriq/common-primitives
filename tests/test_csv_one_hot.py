import unittest
import os
import numpy as np

from common_primitives import csv_feature_extractor, one_hot_maker


class CSVOneHotTestCase(unittest.TestCase):
    def test_produce(self):
        csv = csv_feature_extractor.CSVFeatureExtractor(hyperparams=csv_feature_extractor.Hyperparams.defaults())
        test_file = os.path.dirname(os.path.realpath(__file__)) + "/test_csv/test.csv"
        data = csv.produce(inputs=[test_file]).value

        one_hot = one_hot_maker.OneHotMaker(hyperparams=one_hot_maker.Hyperparams.defaults())
        one_hot.set_training_data(inputs=data)
        one_hot.fit()

        ground_truth = np.array([[1.,  1.,  1.,  0.,  0.],
                                 [2.,  2.,  0.,  1.,  0.],
                                 [3.,  3.,  0.,  0.,  1.]])
        assert np.array_equal(one_hot.produce(inputs=data).value,
                              ground_truth)


if __name__ == '__main__':
    unittest.main()
