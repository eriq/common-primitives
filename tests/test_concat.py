import json
import math
import os
import unittest
import numpy as np
from d3m import container

from common_primitives import concat


class ExtractColumnsByDtypePrimitiveTestCase(unittest.TestCase):
    def test_basic(self):

        test_data_inputs = {'col1': [1.0, 2.0, 3.0]}
        dataframe_inputs = container.DataFrame(data=test_data_inputs)

        test_data_targets = {'col2':[1, 2 ,3]}
        dataframe_targets = container.DataFrame(data=test_data_targets)

        hyperparams_class = concat.ConcatPrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
        primitive = concat.ConcatPrimitive(hyperparams=hyperparams_class.defaults())

        call_result = primitive.produce(inputs=dataframe_inputs, targets=dataframe_targets)

        dataframe_concat = call_result.value

        self.assertEqual( dataframe_concat.as_matrix().tolist(), [[1.0, 1.0], [2.0, 2.0], [3.0, 3.0]])


if __name__ == '__main__':
    unittest.main()
