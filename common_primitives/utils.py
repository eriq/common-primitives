import math
import typing
from typing import *

import frozendict  # type: ignore
import torch  # type: ignore
from torch.autograd import Variable  # type: ignore
import numpy as np  # type: ignore

from d3m.container.numpy import ndarray
from d3m.metadata import base as metadata_base, hyperparams as metadata_hyperparams


def add_dicts(dict1: typing.Dict, dict2: typing.Dict) -> typing.Dict:
    summation = {}
    for key in dict1:
        summation[key] = dict1[key] + dict2[key]
    return summation


def sum_dicts(dictArray: typing.Sequence[typing.Dict]) -> typing.Dict:
    assert len(dictArray) > 0
    summation = dictArray[0]
    for dictionary in dictArray:
        summation = add_dicts(summation, dictionary)
    return summation


def to_variable(value: Any, requires_grad: bool = False) -> Variable:
    """
    Converts an input to torch Variable object
    input
    -----
    value - Type: scalar, Variable object, torch.Tensor, numpy ndarray
    requires_grad  - Type: bool . If true then we require the gradient of that object

    output
    ------
    torch.autograd.variable.Variable object
    """

    if isinstance(value, Variable):
        return value
    elif torch.is_tensor(value):
        return Variable(value.float(), requires_grad=requires_grad)
    elif isinstance(value, np.ndarray) or isinstance(value, ndarray):
        return Variable(torch.from_numpy(value.astype(float)).float(), requires_grad=requires_grad)
    elif value is None:
        return None
    else:
        return Variable(torch.Tensor([float(value)]), requires_grad=requires_grad)


def to_tensor(value: Any) -> torch.FloatTensor:
    """
    Converts an input to a torch FloatTensor
    """
    if isinstance(value, np.ndarray):
        return torch.from_numpy(value).float()
    else:
        raise ValueError('Unsupported type: {}'.format(type(value)))


def refresh_node(node: Variable) -> Variable:
    return torch.autograd.Variable(node.data, True)


def log_mvn_likelihood(mean: torch.FloatTensor, covariance: torch.FloatTensor, observation: torch.FloatTensor) -> torch.FloatTensor:
    """
    all torch primitives
    all non-diagonal elements of covariance matrix are assumed to be zero
    """
    k = mean.shape[0]
    variances = covariance.diag()
    log_likelihood = 0
    for i in range(k):
        log_likelihood += - 0.5 * torch.log(variances[i]) \
                          - 0.5 * k * math.log(2 * math.pi) \
                          - 0.5 * ((observation[i] - mean[i])**2 / variances[i])
    return log_likelihood


def covariance(data: torch.FloatTensor) -> torch.FloatTensor:
    """
    input: NxD torch array
    output: DxD torch array

    calculates covariance matrix of input
    """

    N, D = data.size()
    cov = torch.zeros([D, D]).type(torch.DoubleTensor)
    for contribution in (torch.matmul(row.view(D, 1),
                         row.view(1, D))/N for row in data):
        cov += contribution
    return cov


def remove_mean(data: torch.FloatTensor) -> typing.Tuple[torch.FloatTensor, torch.FloatTensor]:
    """
    input: NxD torch array
    output: D-length mean vector, NxD torch array

    takes a torch tensor, calculates the mean of each
    column and subtracts it

    returns (mean, zero_mean_data)
    """

    N, D = data.size()
    mean = torch.zeros([D]).type(torch.DoubleTensor)
    for row in data:
        mean += row.view(D)/N
    zero_mean_data = data - mean.view(1, D).expand(N, D)
    return mean, zero_mean_data


def denumpify(unknown_object: typing.Any) -> typing.Any:
    """
    changes 'numpy.int's and 'numpy.float's etc to standard Python equivalents
    no effect on other data types
    """
    try:
        return unknown_object.item()
    except AttributeError:
        return unknown_object


M = typing.TypeVar('M', bound=metadata_base.Metadata)


# A copy of "Metadata._query" which starts ignoring "ALL_ELEMENTS" only at a certain depth.
# TODO: Make this part of metadata API.
def _query(selector: metadata_base.Selector, metadata_entry: typing.Optional[metadata_base.MetadataEntry], ignore_all_elements: int = None) -> frozendict.FrozenOrderedDict:
    if metadata_entry is None:
        return frozendict.FrozenOrderedDict()
    if len(selector) == 0:
        return metadata_entry.metadata

    segment, selector_rest = selector[0], selector[1:]

    if ignore_all_elements is not None:
        new_ignore_all_elements = ignore_all_elements - 1
    else:
        new_ignore_all_elements = None

    all_elements_metadata = _query(selector_rest, metadata_entry.all_elements, new_ignore_all_elements)
    if segment is metadata_base.ALL_ELEMENTS:
        metadata = all_elements_metadata
    elif segment in metadata_entry.elements:
        segment = typing.cast(metadata_base.SimpleSelectorSegment, segment)
        metadata = _query(selector_rest, metadata_entry.elements[segment], new_ignore_all_elements)
        if ignore_all_elements is None or ignore_all_elements > 0:
            metadata = metadata_base.Metadata()._merge_metadata(all_elements_metadata, metadata)
    elif ignore_all_elements is not None and ignore_all_elements <= 0:
        metadata = frozendict.FrozenOrderedDict()
    else:
        metadata = all_elements_metadata

    return metadata


# TODO: Make this part of metadata API.
def copy_elements_metadata(source_metadata: metadata_base.Metadata, selector_prefix: metadata_base.Selector,
                           selector: metadata_base.Selector, target_metadata: M, *, source: typing.Any = None) -> M:
    """
    Recursively copies metadata from source to target, starting at the elements of the supplied "selector_prefix" + "selector".
    It does not copy metadata at the "selector_prefix" + "selector" itself.
    """

    # "ALL_ELEMENTS" is always first, if it exists, which works in our favor here.
    elements = source_metadata.get_elements(list(selector_prefix) + list(selector))

    for element in elements:
        new_selector = list(selector) + [element]
        metadata = _query(list(selector_prefix) + new_selector, source_metadata._current_metadata, len(selector_prefix))
        target_metadata = target_metadata.update(new_selector, metadata, source=source)
        target_metadata = copy_elements_metadata(source_metadata, selector_prefix, new_selector, target_metadata, source=source)

    return target_metadata


# TODO: Make this part of metadata API.
def metadata_select_columns(source_metadata: metadata_base.DataMetadata, columns: typing.Sequence[metadata_base.SimpleSelectorSegment], *,
                            source: typing.Any = None) -> metadata_base.DataMetadata:
    # Do nothing if selecting all columns.
    if source_metadata.query((metadata_base.ALL_ELEMENTS,)).get('dimension', {}).get('length', None) == len(columns):
        return source_metadata

    # This makes a copy.
    output_metadata = source_metadata.update(
        (metadata_base.ALL_ELEMENTS,),
        {
            'dimension': {
                'length': len(columns),
            },
        },
        source=source,
    )

    # TODO: Do this better. This change is missing an entry in metadata log.
    elements = output_metadata._current_metadata.all_elements.elements
    output_metadata._current_metadata.all_elements.elements = {}
    for i, column_index in enumerate(sorted(columns)):
        if column_index in elements:
            # If "column_index" is really numeric, we re-enumerate it.
            if isinstance(column_index, int):
                output_metadata._current_metadata.all_elements.elements[i] = elements[column_index]
            else:
                output_metadata._current_metadata.all_elements.elements[column_index] = elements[column_index]

    return output_metadata


# TODO: Make this part of metadata API.
def metadata_list_columns_with_semantic_types(metadata: metadata_base.DataMetadata, semantic_types: typing.Sequence[str]) -> typing.Sequence[int]:
    """
    This is similar to "get_columns_with_semantic_type", but it returns all columns indices which should
    exist in a dimension and not just those which exist in metadata elements (so it returns also columns
    which do not have metadata, and it does not return metadata for "ALL_ELEMENTS").
    """

    columns = []

    for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,)).get('dimension', {}).get('length', 0)):
        if any(metadata.has_semantic_type((metadata_base.ALL_ELEMENTS, column_index), semantic_type) for semantic_type in semantic_types):
            columns.append(column_index)

    return columns


def get_columns_to_produce(metadata: metadata_base.DataMetadata, hyperparams: metadata_hyperparams.Hyperparams, can_produce_column: typing.Callable) -> typing.Tuple[typing.Sequence[int], typing.Sequence[int]]:
    columns_to_produce = list(hyperparams['use_columns'])

    if columns_to_produce:
        return (columns_to_produce, [])

    all_columns = list(range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']))

    all_columns = [column_index for column_index in all_columns if column_index not in hyperparams['exclude_columns']]

    columns_to_produce = []
    columns_not_to_produce = []
    for column_index in all_columns:
        if can_produce_column(column_index):
            columns_to_produce.append(column_index)
        else:
            columns_not_to_produce.append(column_index)

    return (columns_to_produce, columns_not_to_produce)
