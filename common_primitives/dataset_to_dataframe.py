import os
import typing

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('DatasetToDataFramePrimitive',)

Inputs = container.Dataset
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    dataframe_resource = hyperparams.Hyperparameter[typing.Union[str, None]](
        None,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="If there are multiple resources inside a Dataset, which one to extract?",
    )


class DatasetToDataFramePrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which extracts a DataFrame out of a Dataset.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '4b42ce1e-9b98-4a25-b68e-fad13311eb65',
            'version': '0.3.0',
            'name': "Extract a DataFrame from a Dataset",
            'python_path': 'd3m.primitives.datasets.DatasetToDataFrame',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        dataframe_resource_id = self.hyperparams['dataframe_resource']

        if dataframe_resource_id is None:
            if len(inputs) != 1:
                raise ValueError("A Dataset with multiple resources and no DataFrame resource specified as a hyper-parameter.")

            dataframe_resource_id, dataframe = list(inputs.items())[0]

        else:
            dataframe = inputs[dataframe_resource_id]

        if not isinstance(dataframe, container.DataFrame):
            raise TypeError("A Dataset resource is not a DataFrame.")

        dataframe.metadata = self._update_metadata(inputs.metadata, dataframe_resource_id, dataframe, self)

        return base.CallResult(dataframe)

    @classmethod
    def _update_metadata(cls, metadata: metadata_base.DataMetadata, resource_id: str, for_value: typing.Optional[container.DataFrame], source: typing.Any) -> metadata_base.DataMetadata:
        resource_metadata = dict(metadata.query((resource_id,)))

        # Otherwise this will be set when automatically generating metadata.
        if for_value is None:
            resource_metadata.update(
                {
                    'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                    'structural_type': container.DataFrame,
                },
            )

        new_metadata = metadata.clear(resource_metadata, for_value=for_value, source=source)

        new_metadata = utils.copy_elements_metadata(metadata, (resource_id,), (), new_metadata, source=source)

        # Resource is not anymore an entry point.
        new_metadata = new_metadata.remove_semantic_type((), 'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint', source=source)

        return new_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        dataframe_resource_id = hyperparams['dataframe_resource']

        if dataframe_resource_id is None:
            if inputs_metadata.query(())['dimension']['length'] != 1:
                raise ValueError("A Dataset with multiple resources and no DataFrame resource specified as a hyper-parameter.")

            # This can be also "ALL_ELEMENTS" and it will work out, but we prefer a direct resource ID,
            # if available (so we reverse the list and pick the second element, because the first is
            # "ALL_ELEMENTS" if it exists.
            dataframe_resource_id = list(reversed(inputs_metadata.get_elements(())))[0]

        return cls._update_metadata(inputs_metadata, dataframe_resource_id, None, cls)
