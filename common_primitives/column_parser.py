import copy
import hashlib
import os
import typing

import dateutil.parser

from d3m import container, utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives

__all__ = ('ColumnParserPrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    pass


class ColumnParserPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which parses strings into their parsed values.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'd510cb7a-1782-4f51-b44c-58f0236e47c7',
            'version': '0.2.0',
            'name': "Parses strings into their types",
            'python_path': 'd3m.primitives.data.ColumnParser',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        metadata, outputs = self._produce(inputs.metadata, copy.copy(inputs), self)

        outputs.metadata = metadata.set_for_value(outputs)

        return base.CallResult(outputs)

    @classmethod
    def _produce(cls, outputs_metadata: metadata_base.DataMetadata, outputs_data: typing.Optional[Outputs],
                 source: typing.Any) -> typing.Tuple[metadata_base.DataMetadata, typing.Optional[Outputs]]:
        for column_index in outputs_metadata.get_elements((metadata_base.ALL_ELEMENTS,)):
            if column_index is metadata_base.ALL_ELEMENTS:
                continue

            column_index = typing.cast(metadata_base.SimpleSelectorSegment, column_index)

            column_metadata = outputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = column_metadata.get('semantic_types', [])
            if column_metadata['structural_type'] == str:
                if 'http://schema.org/Boolean' in semantic_types:
                    if outputs_data is not None:
                        outputs_data = cls._parse_boolean_data(outputs_data, column_index)

                    outputs_metadata = cls._parse_boolean_metadata(outputs_metadata, column_index, source)

                # Skip parsing if a column is categorical, but also a target column.
                elif 'https://metadata.datadrivendiscovery.org/types/CategoricalData' in semantic_types and 'https://metadata.datadrivendiscovery.org/types/Target' not in semantic_types:
                    if outputs_data is not None:
                        outputs_data = cls._parse_categorical_data(outputs_data, column_index)

                    outputs_metadata = cls._parse_categorical_metadata(outputs_metadata, column_index, source)

                elif 'http://schema.org/Integer' in semantic_types:
                    # For these column types we know all values have to exist so we can assume they can always be represented as integers.
                    if 'https://metadata.datadrivendiscovery.org/types/PrimaryKey' in semantic_types or 'https://metadata.datadrivendiscovery.org/types/UniqueKey' in semantic_types:
                        integer_required = True
                    else:
                        integer_required = False

                    if outputs_data is not None:
                        outputs_data, outputs_metadata = cls._parse_integer(outputs_data, outputs_metadata, column_index, integer_required, source)
                    else:
                        # Without data we assume we can parse everything into integers. This might not be true and
                        # we might end up parsing into floats if we have to represent missing (or invalid) values.
                        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS, column_index), {'structural_type': int}, source=source)

                elif 'http://schema.org/Float' in semantic_types:
                    if outputs_data is not None:
                        outputs_data = cls._parse_float_data(outputs_data, column_index)

                    outputs_metadata = cls._parse_float_metadata(outputs_metadata, column_index, source)

                elif 'http://schema.org/Time' in semantic_types:
                    if outputs_data is not None:
                        outputs_data = cls._parse_time_data(outputs_data, column_index)

                    outputs_metadata = cls._parse_time_metadata(outputs_metadata, column_index, source)

        return outputs_metadata, outputs_data

    @classmethod
    def _parse_boolean_data(cls, outputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        return cls._parse_categorical_data(outputs_data, column_index)

    @classmethod
    def _parse_boolean_metadata(cls, outputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any = None) -> metadata_base.DataMetadata:
        return cls._parse_categorical_metadata(outputs_metadata, column_index, source)

    @classmethod
    def _parse_categorical_data(cls, outputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        values_map: typing.Dict[str, int] = {}
        for value in outputs_data.iloc[:, column_index]:
            value = value.strip()
            if value not in values_map:
                value_hash = hashlib.sha256(value.encode('utf8'))
                values_map[value] = int.from_bytes(value_hash.digest()[0:8], byteorder='little') ^ int.from_bytes(value_hash.digest()[8:16], byteorder='little') ^ \
                    int.from_bytes(value_hash.digest()[16:24], byteorder='little') ^ int.from_bytes(value_hash.digest()[24:32], byteorder='little')

        outputs_data.iloc[:, column_index] = [values_map[value.strip()] for value in outputs_data.iloc[:, column_index]]

        return outputs_data

    @classmethod
    def _parse_categorical_metadata(cls, outputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment,
                                    source: typing.Any) -> metadata_base.DataMetadata:
        return outputs_metadata.update((metadata_base.ALL_ELEMENTS, column_index), {'structural_type': int}, source=source)

    @classmethod
    def _str_to_int(cls, value: str) -> typing.Union[float, int]:
        try:
            return int(value.strip())
        except ValueError:
            try:
                # Maybe it is an int represented as a float. Let's try this. This can get rid of non-integer
                # part of the value, but the integer was requested through a semantic type, so this is probably OK.
                return int(float(value.strip()))
            except ValueError:
                # No luck, use NaN to represent a missing value.
                return float('nan')

    @classmethod
    def _parse_integer(cls, outputs_data: Outputs, outputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment,
                       integer_required: bool, source: typing.Any) -> typing.Tuple[Outputs, metadata_base.DataMetadata]:
        outputs_data.iloc[:, column_index] = [cls._str_to_int(value) for value in outputs_data.iloc[:, column_index]]

        if outputs_data.dtypes[column_index].kind == 'f':
            structural_type: type = float
        elif outputs_data.dtypes[column_index].kind == 'i':
            structural_type = int
        else:
            assert False, outputs_data.dtypes[column_index]

        if structural_type is float and integer_required:
            raise ValueError("Not all values in a column can be parsed into integers, but only integers were expected.")

        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS, column_index), {'structural_type': structural_type}, source=source)

        return outputs_data, outputs_metadata

    @classmethod
    def _str_to_float(cls, value: str) -> float:
        try:
            return float(value.strip())
        except ValueError:
            return float('nan')

    @classmethod
    def _parse_float_data(cls, outputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        outputs_data.iloc[:, column_index] = [cls._str_to_float(value) for value in outputs_data.iloc[:, column_index]]

        return outputs_data

    @classmethod
    def _parse_float_metadata(cls, outputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        return outputs_metadata.update((metadata_base.ALL_ELEMENTS, column_index), {'structural_type': float}, source=source)

    @classmethod
    def _time_to_foat(cls, value: str) -> float:
        try:
            return dateutil.parser.parse(value).timestamp()
        except (ValueError, OverflowError):
            return float('nan')

    @classmethod
    def _parse_time_data(cls, outputs_data: Outputs, column_index: metadata_base.SimpleSelectorSegment) -> Outputs:
        outputs_data.iloc[:, column_index] = [cls._time_to_foat(value) for value in outputs_data.iloc[:, column_index]]

        return outputs_data

    @classmethod
    def _parse_time_metadata(cls, outputs_metadata: metadata_base.DataMetadata, column_index: metadata_base.SimpleSelectorSegment, source: typing.Any) -> metadata_base.DataMetadata:
        return outputs_metadata.update((metadata_base.ALL_ELEMENTS, column_index), {'structural_type': float}, source=source)

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]]) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        return cls._produce(inputs_metadata, None, cls)[0]
