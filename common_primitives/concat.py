import os
import typing
import copy

from numpy import dtype

from d3m import container, utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.container.pandas import DataFrame
from d3m.container.numpy import ndarray

import d3m.container.pandas as pd

import common_primitives

__all__ = ('ConcatPrimitive',)

# Inputs = typing.Union[container.DataFrame, ndarray]
Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    axis = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default = 1,
        description = 'Axis to determine how to concatenate (0: index, 1: Columns)',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )

# TODO add support for list and np.arrays
class ConcatPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which concatenate two dataframes and returns a Dataframe.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'aff6a77a-faa0-41c5-9595-de2e7f7c4760',
            'version': '0.1.0',
            'name': "Concatenate two dataframes",
            'python_path': 'd3m.primitives.data.Concat',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: typing.Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)
        self._axis = self.hyperparams['axis']

    def produce(self, *, inputs: Inputs, targets: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        outputs = self._produce(copy.copy(inputs), copy.copy(targets), self)
        return base.CallResult(outputs)

    def _produce(self, inputs: Inputs, targets: Inputs, source: typing.Any = None) -> typing.Tuple[metadata_base.DataMetadata, typing.Optional[Outputs]]:

        if isinstance(inputs, ndarray):
            inputs = DataFrame(data=inputs)
        
        if isinstance(targets, ndarray):
            targets = DataFrame(data=targets)
        
        outputs_data = pd.pandas.concat([inputs, targets], axis=self._axis)

        return outputs_data

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, targets: Inputs, timeout: float = None, iterations: int = None) -> base.MultiCallResult:  # type: ignore
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, targets=targets)
