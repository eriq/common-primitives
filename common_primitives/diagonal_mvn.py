import time
from typing import NamedTuple, Sequence, Tuple, Optional, Dict
from d3m.container.list import List
import numpy as np # type: ignore
import scipy # type: ignore
import torch # type: ignore
from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
import sys

from d3m.primitive_interfaces.base import (
    ProbabilisticCompositionalityMixin,
    GradientCompositionalityMixin,
    SamplingCompositionalityMixin,
    CallResult,
    Gradients,
    DockerContainer,
)

import os

from d3m.primitive_interfaces.generator import GeneratorPrimitiveBase
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase

from .utils import to_variable, refresh_node, log_mvn_likelihood

Void = type(None)

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
# 1D list of None
Inputs = ndarray
# n_dimensions by n_inputs
Outputs = ndarray


# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
class Hyperparams(hyperparams.Hyperparams):
    alpha = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1e-2,
        description='initial fitting step size'
    )
    beta = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1e-8,
        description='see reference for details'
    )
    batch_size = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1000,
        description='if there is a lot of data, primitive fits using gradient descent in which case, specify batch size here'
    )


class Params(params.Params):
    mean: ndarray
    covariance: ndarray


class DiagonalMVN(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                  GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                  SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                  # Base class should be after all mixins.
                  #  GeneratorPrimitiveBase[Outputs, Params, Hyperparams]): #TODO put this back when the devel metadata branch is merged -> #interface TODO, cannot inherit from this base class anymore due to type issues
                  SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive which allows fitting, and sampling from,
    a multivariate Gaussian (with diagonal covariance matrix)
    """

    __author__ = "Oxford DARPA D3M Team, William Harvey <willh@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
         'id': '56c715a1-6017-48ad-a6ed-fd8275fd3440',
         'version': '0.1.0',
         'name': 'Diagonal multivariate normal distribution primitive, mainly for being used as a weight prior of another primitive.',
         'keywords': ['normal', 'distribution'],
         'source': {
            'name': 'common-primitives',
            'contact': 'mailto:willh@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.DiagonalMVN',
         'algorithm_types': ['NUMERICAL_METHOD'],
         'primitive_family': 'OPERATOR',
    })

    # It is important that all hyper-parameters (parameters which do not change during
    # a life-cycle of a primitive) are explicitly listed and typed in the constructor.
    # This allows one to do hyper-parameter tuning and explores the space of
    # hyper-parameters.
    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)
        """
        Optionally set ``alpha``, the initial fitting step size,
        and ``beta``, which is as described in Baydin, Atilim Gunes, et al.,
        'Online Learning Rate Adaptation with Hypergradient Descent',
        arXiv preprint arXiv:1703.04782 (2017).
        """

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._alpha = hyperparams['alpha']
        self._beta  = hyperparams['beta']
        self._batch_size = hyperparams['batch_size']
        self._fit_term_temperature = 0.0
        self._mean = None  # type: torch.autograd.Variable
        self._covariance = None  # type: torch.autograd.Variable
        self._training_outputs = None  # type: torch.autograd.Variable
        self._new_training_outputs = True
        self._fitted = True
        self._iterations_done = None  # type: Optional[int]

    def backward(self, *,
                 gradient_outputs: Gradients[Outputs],
                 fine_tune: bool = False,
                 fine_tune_learning_rate: float = 0.00001,
                 fine_tune_weight_decay: float = 0.00001) -> Tuple[Gradients[Inputs], Gradients[Params]]: # type: ignore
        # TODO fix this fit term temp
        pass

    # Our best choice is mean.
    def _produce_one(self) -> ndarray:
        return self._mean.data.numpy()

    # Inputs is a list of None values.
    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        if self._mean is None:
            raise ValueError("Missing parameter 'mean' - call 'fit' first")
        self._fitted = True
        self._iterations_done = None
        p = np.array([self._produce_one() for _ in inputs])
        return CallResult(p, has_finished=False, iterations_done=self._iterations_done)

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_outputs = to_variable(outputs, requires_grad=True)
        self._new_training_outputs = True

    # Example of a fit method which can be iteratively called multiple times.
    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        """
        Fits parameters to MLE. Runs gradient descent for ``timeout`` seconds or ``iterations``
        iterations, whichever comes sooner, on log likelihood of training data.
        """
        if self._training_outputs is None:
            raise ValueError("Missing training data.")

        # if its low dimensional and not much data
        if sum(self._training_outputs.shape) < 10000:
            nd = self._training_outputs.data.numpy()

            self._mean = torch.Tensor(np.average(nd, axis=0))
            self._covariance = torch.Tensor(np.diag(np.square(np.std(nd, axis=0))))
            self._iterations_done = iterations
            self._new_training_outputs = False

            return CallResult(None)

        if timeout is None:
            timeout = np.inf
        if iterations is None:
            iterations = 100

        # Initializing fitting from scratch.
        if self._new_training_outputs:
            self._mean = to_variable(np.zeros(self._training_outputs.size()[0]), True)
            self._covariance = to_variable(np.eye(self._training_outputs.size()[0]), True)
            self._new_training_outputs = False

        start = time.time()
        # We can always iterate more, even if not reasonable.
        self._fitted = False
        self._iterations_done = 0
        prev_mean_grad, prev_covariance_grad = None, None  # type: torch.Tensor, torch.Tensor

        while time.time() < start + timeout and self._iterations_done < iterations:
            self._iterations_done += 1

            data_count = len(self._training_outputs)
            if data_count > self._batch_size:
                sample = self._training_outputs.data.numpy()[np.random.choice(data_count, self._batch_size), :]
            else:
                sample = self._training_outputs

            log_likelihood = sum(self._log_likelihood(output=training_output)
                                 for training_output in sample)

            self._mean.retain_grad()
            self._covariance.retain_grad()
            log_likelihood.backward() # type: ignore

            mean_grad = self._mean.grad.data
            covariance_grad = self._covariance.grad.data

            if prev_mean_grad is not None:
                self._alpha += self._beta * \
                              (torch.dot(mean_grad, prev_mean_grad)
                               + torch.dot(covariance_grad.view(-1),
                                           prev_covariance_grad.view(-1)))
            prev_mean_grad, prev_covariance_grad = mean_grad, covariance_grad

            self._mean.data += mean_grad * self._alpha / torch.norm(mean_grad)
            self._covariance.data += covariance_grad * self._alpha / torch.norm(prev_covariance_grad)

            self._mean = refresh_node(self._mean)
            self._covariance = refresh_node(self._covariance)
        return CallResult(None)

    def get_params(self) -> Params:
        return Params(mean=ndarray(self._mean.data.numpy()), covariance=ndarray(self._covariance.data.numpy()))

    def set_params(self, *, params: Params) -> None:
        self._mean = to_variable(params['mean'], requires_grad=True)
        self._covariance = to_variable(params['covariance'], requires_grad=True)

    # sample_once returns one sample from the underlying distribution of
    # outputs, while produce_one returns the best output as understood by the primitive.
    def _sample_once(self, *, inputs: Inputs) -> Outputs:
        mean = self._mean.data.numpy()
        covariance = self._covariance.data.numpy()
        return np.array([np.random.multivariate_normal(mean, covariance) for _ in inputs])

    def sample(self, *, inputs: Inputs, num_samples: int = 1, timeout: float = None, iterations: int = None) -> CallResult[Sequence[Outputs]]:
        # sample just returns a number of samples from the current mvn
        s = np.array([self._sample_once(inputs=inputs) for _ in range(num_samples)])
        return CallResult(s, has_finished=False, iterations_done=self._iterations_done)

    def _log_likelihood(self, *, output:  torch.autograd.Variable) -> torch.autograd.Variable:
        """
        Calculates log(normal_density(self._mean, self._covariance)).
        """

        output = to_variable(output)
        return log_mvn_likelihood(self._mean, self._covariance, output)

    def _gradient_output_log_likelihood(self, *, output:  torch.autograd.Variable) -> torch.autograd.Variable:
        """
        Output is D-length torch variable.
        """

        output = refresh_node(output)
        log_likelihood = self._log_likelihood(output=output)
        log_likelihood.backward()
        return output.grad

    def _gradient_params_log_likelihood(self, *, output:  torch.autograd.Variable) -> Tuple[torch.autograd.Variable, torch.autograd.Variable]:
        """
        Output is D-length torch variable.
        """

        self._mean = refresh_node(self._mean)
        self._covariance = refresh_node(self._covariance)
        log_likelihood = self._log_likelihood(output=output)
        log_likelihood.backward()
        return (self._mean.grad, self._covariance.grad)

    def log_likelihoods(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[ndarray]: # type: ignore
        result = np.array([self._log_likelihood(output=output).data.numpy() for output in outputs])
        return CallResult(result, has_finished=False, iterations_done=self._iterations_done)

    def log_likelihood(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[float]:
        """
        Calculates log(normal_density(self._mean, self._covariance)).
        """

        result = self.log_likelihoods(outputs=outputs, inputs=inputs, timeout=timeout, iterations=iterations)

        return CallResult(sum(result.value), has_finished=result.has_finished, iterations_done=result.iterations_done)

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]:  # type: ignore
        # TODO update comment reflect inibatch
        """
        Calculates gradient of log(normal_density(self._mean, self._covariance)) * fit_term_temperature with respect to output.
        """

        outputs_vars = [to_variable(output, requires_grad=True) for output in outputs]

        grad = sum(self._gradient_output_log_likelihood(output=output)
                   for output in outputs_vars)


        return grad.data.numpy() # type: ignore

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Params]:  # type: ignore
        """
        Calculates gradient of log(normal_density(self._mean, self._covariance)) * fit_term_temperature with respect to params.
        """

        outputs_vars = [to_variable(output, requires_grad=True) for output in outputs]

        grads = [self._gradient_params_log_likelihood(output=output)
                 for output in outputs_vars]
        grad_mean = sum(grad[0] for grad in grads)
        grad_covariance = sum(grad[1] for grad in grads)


        return Params(mean=ndarray(grad_mean.data.numpy()), covariance=ndarray(grad_covariance.data.numpy())) # type: ignore

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        self._fit_term_temperature = temperature

    def get_call_metadata(self) -> CallResult: # type: ignore
        return CallResult(None, has_finished=False, iterations_done=self._iterations_done)
