import os
import typing
import copy

from numpy import dtype

from d3m import container, utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer
from d3m.primitive_interfaces.base import CallResult, DockerContainer

import common_primitives

__all__ = ('ExtractColumnsByDtypePrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    filter_type = hyperparams.Hyperparameter[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default = 'include',
        description = 'Type of filter to use (include, exclude).',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )
    dtype_filter = hyperparams.Hyperparameter[str](
        default = 'uint64',
        description = 'Filter all columns from a datafram that have this dtype.',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter']
    )


class ExtractColumnsByDtypePrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which returns a Dataframe according with select_dtypes.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'da8caa47-f186-4214-898b-4298cda46cf4',
            'version': '0.2.0',
            'name': "Select columns according with their dtypes",
            'python_path': 'd3m.primitives.data.ExtractColumnsByDtype',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def __init__(self, *, hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: typing.Dict[str, DockerContainer] = None) -> None:
        super().__init__(hyperparams=hyperparams,
                         random_seed=random_seed,
                         docker_containers=docker_containers)
        self._dtype_filter = self.hyperparams['dtype_filter']
        self._filter_type = self.hyperparams['filter_type']

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        outputs = self._produce(inputs.metadata, copy.copy(inputs), self)

        return base.CallResult(outputs)

    def _produce(self, outputs_metadata: metadata_base.DataMetadata, outputs_data: typing.Optional[Outputs],
                 source: typing.Any = None) -> typing.Tuple[metadata_base.DataMetadata, typing.Optional[Outputs]]:

        select_dtypes_params = {self._filter_type: self._dtype_filter}
        if dtype(self._dtype_filter) in outputs_data.dtypes.values:
            outputs_data = outputs_data.select_dtypes(**select_dtypes_params)

        return outputs_data

