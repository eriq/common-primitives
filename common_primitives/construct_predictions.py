import os
import typing

import pandas  # type: ignore

from d3m import container, utils
from d3m.metadata import base as metadata_base, hyperparams, params
from d3m.primitive_interfaces import base

import common_primitives

__all__ = ('ConstructPredictionsPrimitive',)

Inputs = container.DataFrame
Targets = typing.Union[container.DataFrame, container.ndarray]
Outputs = container.DataFrame


class Params(params.Params):
    target_names: typing.List[str]


class Hyperparams(hyperparams.Hyperparams):
    pass


class ConstructPredictionsPrimitive(base.PrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    A primitive which takes as input a numpy array with results from wrapped sklearn primitive
    and outputs a DataFrame suitable for pipeline output.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '8d38b340-f83f-4877-baaa-162f8e551736',
            'version': '0.2.0',
            'name': "Construct pipeline predictions output",
            'python_path': 'd3m.primitives.data.ConstructPredictions',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def __init__(self, *, hyperparams: Hyperparams) -> None:
        super().__init__(hyperparams=hyperparams)

        self._target_names: typing.List[str] = []
        self._inputs: Inputs = None

    # TODO: When copying metadata, copy also all individual metadata for columns and rows, and any recursive metadata for nested data.
    def produce(self, *, inputs: Inputs, targets: Targets, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # type: ignore
        """
        When producing, ``inputs`` is a ``DataFrame`` before attributes were extracted.
        It should contain all samples which are then passed to a wrapped sklearn primitive.
        It is used to get ``d3mIndex`` column. When producing at testing phase the
        ``inputs`` do not contain targets so we had to store names of target columns
        during fitting.

        ``targets`` is the output from the wrapper sklearn primitive.
        """

        index_column = self._get_index_column(inputs.metadata)

        index = inputs.iloc[:, index_column]
        index.metadata = self._select_columns(inputs.metadata, [index_column], source=self)
        index.metadata = index.metadata.set_for_value(index)

        # Making sure we are operating on a dataframe. We assume any metadata is copied over.
        # See: https://gitlab.com/datadrivendiscovery/d3m/issues/76
        targets_dataframe = container.DataFrame(targets)

        if len(self._target_names) != len(targets_dataframe.columns):
            raise ValueError("Not all expected target columns provided.")

        # Number of samples have to match.
        dataframe = pandas.concat((index, targets_dataframe), axis=1)
        dataframe.metadata = index.metadata.set_for_value(dataframe)

        assert len(dataframe.columns) == 1 + len(self._target_names)

        dataframe.metadata = self._update_metadata(dataframe.metadata, targets_dataframe.metadata, self._target_names, self)

        return base.CallResult(dataframe)

    def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, targets: Targets, timeout: float = None, iterations: int = None) -> base.MultiCallResult:  # type: ignore
        return self._multi_produce(produce_methods=produce_methods, timeout=timeout, iterations=iterations, inputs=inputs, targets=targets)

    def set_training_data(self, *, inputs: Inputs) -> None:  # type: ignore
        """
        ``inputs`` is in this case a ``DataFrame`` before attributes and targets were
        extracted.
        """

        self._inputs = inputs

    def fit(self, *, timeout: float = None, iterations: int = None) -> base.CallResult[None]:
        """
        During fitting, names of target columns as stored.
        """

        self._target_names = self._get_target_names(self._inputs.metadata)

        return base.CallResult(None)

    def get_params(self) -> Params:
        return Params(target_names=self._target_names)

    def set_params(self, *, params: Params) -> None:
        self._target_names = params['target_names']

    @classmethod
    def _update_metadata(cls, metadata: metadata_base.DataMetadata, targets_metadata: metadata_base.DataMetadata, target_names: typing.List[str], source: typing.Any) -> metadata_base.DataMetadata:
        metadata = metadata.update(
            (metadata_base.ALL_ELEMENTS,),
            {
                'dimension': {
                    'length': 1 + len(target_names),
                },
            },
            source=source,
        )

        # Copy over metadata for targets.
        targets_dataframe_columns_length = targets_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        for column_index in range(targets_dataframe_columns_length):
            column_metadata = dict(targets_metadata.query((metadata_base.ALL_ELEMENTS, column_index)))

            # Making editable.
            if 'semantic_types' not in column_metadata:
                column_metadata['semantic_types'] = []
            else:
                column_metadata['semantic_types'] = list(column_metadata['semantic_types'])

            # Resource is not anymore a true target, but predicted target.
            if 'https://metadata.datadrivendiscovery.org/types/Target' not in column_metadata['semantic_types']:
                column_metadata['semantic_types'].append('https://metadata.datadrivendiscovery.org/types/Target')
            if 'https://metadata.datadrivendiscovery.org/types/PredictedTarget' not in column_metadata['semantic_types']:
                column_metadata['semantic_types'].append('https://metadata.datadrivendiscovery.org/types/PredictedTarget')
            if 'https://metadata.datadrivendiscovery.org/types/TrueTarget' in column_metadata['semantic_types']:
                column_metadata['semantic_types'] = [
                    semantic_type for semantic_type in column_metadata['semantic_types'] if semantic_type != 'https://metadata.datadrivendiscovery.org/types/TrueTarget'
                ]

            metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index + 1), column_metadata, source=source)

        # Set target columns names.
        for i, target_name in enumerate(target_names):
            metadata = metadata.update((metadata_base.ALL_ELEMENTS, i + 1), {
                'name': target_name,
            }, source=source)

        return metadata

    @classmethod
    def _get_target_names(cls, metadata: metadata_base.DataMetadata) -> typing.List[str]:
        target_names = []

        for column_index in metadata.get_elements((metadata_base.ALL_ELEMENTS,)):
            if column_index is metadata_base.ALL_ELEMENTS:
                continue

            column_index = typing.cast(metadata_base.SimpleSelectorSegment, column_index)

            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = column_metadata.get('semantic_types', [])

            # We extract targets in order.
            if 'https://metadata.datadrivendiscovery.org/types/Target' in semantic_types:
                target_names.append(column_metadata.get('name', str(column_index)))

        return target_names

    @classmethod
    def _get_index_column(cls, metadata: metadata_base.DataMetadata) -> metadata_base.SimpleSelectorSegment:
        for column_index in metadata.get_elements((metadata_base.ALL_ELEMENTS,)):
            if column_index is metadata_base.ALL_ELEMENTS:
                continue

            column_index = typing.cast(metadata_base.SimpleSelectorSegment, column_index)

            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            semantic_types = column_metadata.get('semantic_types', [])

            if 'https://metadata.datadrivendiscovery.org/types/PrimaryKey' in semantic_types:
                return column_index

        raise ValueError("Input data has no index column.")

    # TODO: Make this part of metadata API.
    @classmethod
    def _select_columns(cls, source_metadata: metadata_base.DataMetadata, columns: typing.Sequence[metadata_base.SimpleSelectorSegment], *,
                        source: typing.Any = None) -> metadata_base.DataMetadata:
        if source is None:
            source = cls

        # This makes a copy.
        output_metadata = source_metadata.update(
            (metadata_base.ALL_ELEMENTS,),
            {
                'dimension': {
                    'length': len(columns),
                },
            },
            source=source,
        )

        # TODO: Do this better. This change is missing an entry in metadata log.
        elements = output_metadata._current_metadata.all_elements.elements
        output_metadata._current_metadata.all_elements.elements = {}
        for i, column_index in enumerate(sorted(columns)):
            if column_index in elements:
                # If "column_index" is really numeric, we re-enumerate it.
                if isinstance(column_index, int):
                    output_metadata._current_metadata.all_elements.elements[i] = elements[column_index]
                else:
                    output_metadata._current_metadata.all_elements.elements[column_index] = elements[column_index]

        return output_metadata

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]]) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments or 'targets' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])
        targets_metadata = typing.cast(metadata_base.DataMetadata, arguments['targets'])

        index_column = cls._get_index_column(inputs_metadata)
        target_names = cls._get_target_names(inputs_metadata)

        if len(target_names) != targets_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']:
            raise ValueError("Not all expected target columns provided.")

        outputs_metadata = cls._select_columns(inputs_metadata, [index_column], source=cls)

        outputs_metadata = cls._update_metadata(outputs_metadata, targets_metadata, target_names, cls)

        return outputs_metadata
