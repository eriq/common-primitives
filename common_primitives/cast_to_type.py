import decimal
import numbers
import os
import typing

import numpy  # type: ignore

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

import common_primitives
from common_primitives import utils

__all__ = ('CastToTypePrimitive',)

Inputs = container.DataFrame
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    type_to_cast = hyperparams.Enumeration[str](
        values=['str', 'float'],
        default='str',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
    )
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on, applicable if \"use_columns\" is not provided.",
    )


class CastToTypePrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which casts all columns it can cast (by default, controlled by ``use_columns``,
    ``exclude_columns``) of an input DataFrame to a given structural type (dtype).
    It removes columns which are not cast.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'eb5fe752-f22a-4090-948b-aafcef203bf5',
            'version': '0.2.0',
            'name': "Casts DataFrame",
            'python_path': 'd3m.primitives.data.CastToType',
            'source': {
               'name': common_primitives.__author__,
            },
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    _type_map = {
        'str': str,
        'float': float,
    }

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        type_to_cast = self._type_map[self.hyperparams['type_to_cast']]

        columns_to_produce = self._get_columns(inputs.metadata, type_to_cast, self.hyperparams)

        outputs = inputs.iloc[:, columns_to_produce].astype(type_to_cast)
        outputs_metadata = utils.metadata_select_columns(inputs.metadata, columns_to_produce, source=self)

        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS), {
            'structural_type': type_to_cast,
        }, source=self)

        outputs.metadata = outputs_metadata.set_for_value(outputs, generate_metadata=False)

        return base.CallResult(outputs)

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, type_to_cast: type) -> bool:
        if type_to_cast == str:
            # TODO: Anything can be converted to string, but is it meaningful (Python string description of object might not be)? Should we limit what can be cast this way?
            return True
        else:
            column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            structural_type = column_metadata.get('structural_type', None)

            if structural_type is None:
                return False

            # "d3m_utils.is_subclass" is respecting Python numeric tower.
            return d3m_utils.is_subclass(structural_type, typing.Union[float, int, numpy.integer, numpy.float64, numbers.Integral, decimal.Decimal, numbers.Real])

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, type_to_cast: type, hyperparams: hyperparams.Hyperparams) -> typing.Sequence[int]:
        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, type_to_cast)

        columns_to_produce, columns_not_to_produce = utils.get_columns_to_produce(inputs_metadata, hyperparams, can_produce_column)

        if not columns_to_produce:
            raise ValueError("No columns to be cast to type '{type}'.".format(type=type_to_cast))

        elif columns_not_to_produce:
            cls.logger.warning("Not all columns can be cast to type '%(type)s'. Dropping columns: %(columns)s", {
                'type': type_to_cast,
                'columns': columns_not_to_produce,
            })

        return columns_to_produce

    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:
        output_metadata = super().can_accept(method_name=method_name, arguments=arguments, hyperparams=hyperparams)

        # If structural types didn't match, don't bother.
        if output_metadata is None:
            return None

        if 'inputs' not in arguments:
            return output_metadata

        inputs_metadata = typing.cast(metadata_base.DataMetadata, arguments['inputs'])

        type_to_cast = cls._type_map[hyperparams['type_to_cast']]

        columns_to_produce = cls._get_columns(inputs_metadata, type_to_cast, hyperparams)

        outputs_metadata = utils.metadata_select_columns(inputs_metadata, columns_to_produce, source=cls)

        outputs_metadata = outputs_metadata.update((metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS), {
            'structural_type': type_to_cast,
        }, source=cls)

        return outputs_metadata
