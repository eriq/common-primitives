from typing import List, Dict, Union, Sequence
import os
import numpy as np
from sklearn.ensemble.forest import RandomForestClassifier as RF  # type: ignore

from d3m.container.numpy import ndarray
from d3m.metadata import hyperparams, params
import d3m.metadata.base as metadata_module
from d3m import utils
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import ProbabilisticCompositionalityMixin
from d3m.primitive_interfaces.base import CallResult, DockerContainer


Inputs = ndarray
Outputs = ndarray


class Params(params.Params):
    estimators: List
    classes: ndarray
    n_classes: Union[int, List]
    n_features: int
    n_outputs: int
    feature_importances: ndarray
    oob_score: float
    oob_decision_function: ndarray


class Hyperparams(hyperparams.Hyperparams):
    # TODO test that these initializations are robust to all possibilities
    n_estimators = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=200,
        description='The number of trees in the forest. '
    )
    criterion = hyperparams.Enumeration[str](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        values=['gini', 'entropy'],
        default='gini',
        description=('The function to measure the quality of a split. '
                     'Supported criteria are "gini" for the Gini impurity '
                     'and "entropy" for the information gain. '
                     'Note: this parameter is tree-specific. '))
    max_features = hyperparams.Hyperparameter[Union[int, None]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=None,
        description='The number of features to consider when looking for the best split:  - If int, then consider `max_features` features at each split. - If float, then `max_features` is a percentage and `int(max_features * n_features)` features are considered at each split. - If "auto", then `max_features=sqrt(n_features)`. - If "sqrt", then `max_features=sqrt(n_features)` (same as "auto"). - If "log2", then `max_features=log2(n_features)`. - If None, then `max_features=n_features`.  Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than ``max_features`` features. '
    )
    max_depth = hyperparams.Hyperparameter[Union[int, None]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=None,
        description='The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples. '
    )
    min_samples_split = hyperparams.UniformInt(
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=2,
        lower=2,
        upper=20,
        description='The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` are the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages. '
    )
    min_samples_leaf = hyperparams.UniformInt(
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1,
        lower=1,
        upper=20,
        description='The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` are the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages. '
    )
    min_weight_fraction_leaf = hyperparams.Hyperparameter[float](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=0,
        description='The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided. '
    )
    max_leaf_nodes = hyperparams.Hyperparameter[Union[int, None]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=None,
        description='Grow trees with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes. '
    )
    min_impurity_split = hyperparams.Hyperparameter[Union[float, None]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=None,
        description='Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.  .. versionadded:: 0.18 '
    )
    bootstrap = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=True,
        description='Whether bootstrap samples are used when building trees. '
    )
    oob_score = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=False,
        description='Whether to use out-of-bag samples to estimate the generalization accuracy. '
    )
    n_jobs = hyperparams.Hyperparameter[int](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=1,
        description='The number of jobs to run in parallel for both `fit` and `predict`. If -1, then the number of jobs is set to the number of cores. '
    )
    warm_start = hyperparams.Hyperparameter[bool](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default=False,
        description='When set to ``True``, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new forest. '
    )
    class_weight = hyperparams.Hyperparameter[Union[str, dict, List]](
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        default='balanced',
        description='"balanced_subsample" or None, optional (default=None) Weights associated with classes in the form ``{class_label: weight}``. If not given, all classes are supposed to have weight one. For multi-output problems, a list of dicts can be provided in the same order as the columns of y.  The "balanced" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``  The "balanced_subsample" mode is the same as "balanced" except that weights are computed based on the bootstrap sample for every tree grown.  For multi-output, the weights of each column of y will be multiplied.  Note that these weights will be multiplied with sample_weight (passed through the fit method) if sample_weight is specified. '
    )


class RandomForestClassifier(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                             SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Primitive wrapping for sklearn.ensemble.forest.RandomForestClassifier
    """

    __author__ = 'Oxford DARPA D3M Team, Rob Zinkov <zinkov@robots.ox.ac.uk>'
    metadata = metadata_module.PrimitiveMetadata({
         "id": "37c2b19d-bdab-4a30-ba08-6be49edcc6af",
         "version": "0.1.0",
         "name": "sklearn.ensemble.forest.RandomForestClassifier",
         "keywords": ['random forest', 'decision tree'],
         "source": {'name': 'common-primitives'},
         'installation': [{'type': metadata_module.PrimitiveInstallationType.PIP,
                           'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(
                               git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                           ),
         }],
         "python_path": "d3m.primitives.common_primitives.RandomForestClassifier",
         "algorithm_types": ['RANDOM_FOREST'],
         "primitive_family": "CLASSIFICATION"
    })
    # TODO think about adding the samppling mixin

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._clf = RF(
            n_estimators=self.hyperparams['n_estimators'],
            criterion=self.hyperparams['criterion'],
            max_features=self.hyperparams['max_features'],
            max_depth=self.hyperparams['max_depth'],
            min_samples_split=self.hyperparams['min_samples_split'],
            min_samples_leaf=self.hyperparams['min_samples_leaf'],
            min_weight_fraction_leaf=self.hyperparams['min_weight_fraction_leaf'],
            max_leaf_nodes=self.hyperparams['max_leaf_nodes'],
            min_impurity_split=self.hyperparams['min_impurity_split'],
            bootstrap=self.hyperparams['bootstrap'],
            oob_score=self.hyperparams['oob_score'],
            n_jobs=self.hyperparams['n_jobs'],
            warm_start=self.hyperparams['warm_start'],
            class_weight=self.hyperparams['class_weight']
        )
        self._training_inputs = None
        self._training_outputs = None
        self._fitted = False

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs = inputs
        self._training_outputs = outputs
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")

        # TODO what should happen here when timeout is reached (wrap in a thread)
        self._clf.fit(self._training_inputs, self._training_outputs)
        self._fitted = True

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        return CallResult(self._clf.predict(inputs))

    def log_likelihoods(self, *, outputs: Outputs, inputs: Inputs,
                        timeout: float = None, iterations: int = None) -> CallResult[Sequence[float]]:
        L = self._clf.predict_log_proba(inputs)

    def get_params(self) -> Params:
        return Params(
            estimators=self._clf.estimators_,
            classes=self._clf.classes_,
            n_classes=self._clf.n_classes_,
            n_features=self._clf.n_features_,
            n_outputs=self._clf.n_outputs_,
            feature_importances=self._clf.feature_importances_,
            oob_score=self._clf.oob_score_,
            oob_decision_function=self._clf.oob_decision_function_,
        )

    def set_params(self, *, params: Params) -> None:
        self._clf.estimators_ = params.estimators
        self._clf.classes_ = params.classes
        self._clf.n_classes_ = params.n_classes
        self._clf.n_features_ = params.n_features
        self._clf.n_outputs_ = params.n_outputs
        self._clf.oob_score_ = params.oob_score
        self._clf.oob_decision_function_ = params.oob_decision_function
