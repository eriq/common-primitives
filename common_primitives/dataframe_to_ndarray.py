import os
import typing

import numpy

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams
from d3m.primitive_interfaces import base, transformer

from common_primitives import utils

__all__ = ('DataFrameToNDArrayPrimitive',)

Inputs = container.DataFrame
Outputs = container.ndarray


class Hyperparams(hyperparams.Hyperparams):
    pass


class DataFrameToNDArrayPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which converts a pandas dataframe into a numpy array.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '34f71b2e-17bb-488d-a2ba-b60b8c305539',
            'version': '0.1.0',
            'name': "DataFrame to ndarray converter",
            'python_path': 'd3m.primitives.datasets.DataFrameToNDArrayPrimitive',
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        # transform the dataframe into a numpy array
        record_array = inputs.to_records(index=False)
        numpy_array = container.ndarray(numpy.asarray(record_array))
        # copy the metadata across
        numpy_array.metadata = self._update_metadata(inputs.metadata, numpy_array, self)

        return base.CallResult(numpy_array)

    @classmethod
    def _update_metadata(cls, metadata: metadata_base.DataMetadata, for_value: container.numpy = None, source: typing.Any = None) -> metadata_base.DataMetadata:
        # set the schema version and structural type
        dataset_metadata = dict(metadata.query(()))
        dataset_metadata.update(
            {
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': container.ndarray
            },
        )

        if source is None:
            source = cls

        # create new metadata initialized with base values
        new_metadata = metadata.clear(dataset_metadata, for_value=for_value, source=source)
        new_metadata = utils.copy_elements_metadata(metadata, (), (), new_metadata, source=source)

        return new_metadata
