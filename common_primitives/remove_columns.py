import typing
import os
import json
import sys
import copy

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer
from common_primitives import utils
import numpy
from typing import List, Tuple

__all__ = ('RemoveColumnsPrimitive',)

Inputs = container.Dataset
Outputs = container.Dataset


class Hyperparams(hyperparams.Hyperparams):
    resource_id = hyperparams.Hyperparameter[str](
        default='0',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='resource ID of columns to remove'
    )
    columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[str](''),
        default=(),
        max_size=sys.maxsize,
        min_size=0,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description='names of columns to remove'
    )


class RemoveColumnsPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which removes columns from a dataframe.  Columns are specified by name, comparison is case sensitive
    for consistency with underlying dataframe drop behavior.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '2eeff053-395a-497d-88db-7374c27812e6',
            'version': '0.1.0',
            'name': "Column remover",
            'python_path': 'd3m.primitives.datasets.RemoveColumns',
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.ARRAY_SLICING,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        resource_id = self.hyperparams['resource_id']
        resource = inputs[resource_id]

        num_cols = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS))['dimension']['length']

        # compute the set of columns to keep
        param_column_names = self.hyperparams['columns']
        column_names = set([(inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, i))['name']) for i in range(0, num_cols)])
        keep_columns = column_names.difference(param_column_names)

        # set the column count to reflect the keep list size
        element_metadata = dict(inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS,)))
        dimension_metadata = dict(element_metadata['dimension'])
        dimension_metadata['length'] = len(keep_columns)
        element_metadata['dimension'] = dimension_metadata

        # copy the input metadata
        new_metadata = inputs.metadata.clear(inputs.metadata.query(()), source=inputs)
        new_metadata = utils.copy_elements_metadata(inputs.metadata, (), (), new_metadata)

        new_metadata = new_metadata.update((resource_id, metadata_base.ALL_ELEMENTS,), element_metadata)

        # copy the `keep` columns across
        new_col_num = 0
        for i in range(0, num_cols):
            source_col_metadata = inputs.metadata.query((resource_id, metadata_base.ALL_ELEMENTS, i))
            if source_col_metadata['name'] in keep_columns:
                new_metadata = new_metadata.update((resource_id, metadata_base.ALL_ELEMENTS, new_col_num), source_col_metadata)
                new_col_num += 1

        outputs = copy.copy(inputs)
        outputs[resource_id] = resource.drop(columns=list(param_column_names))
        outputs.metadata = new_metadata

        return base.CallResult(outputs)
