import os
import typing

import numpy  # type: ignore

from d3m import container, utils as d3m_utils
from d3m.metadata import base as metadata_base
from d3m.metadata import hyperparams
from d3m.primitive_interfaces import base, transformer

from common_primitives import utils

__all__ = ('NDArrayToDataFramePrimitive',)

Inputs = container.ndarray
Outputs = container.DataFrame


class Hyperparams(hyperparams.Hyperparams):
    pass


class NDArrayToDataFramePrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
    """
    A primitive which converts numpy array into a pandas dataframe.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'f5241b2e-64f7-44ad-9675-df3d08066437',
            'version': '0.1.0',
            'name': "ndarray to Dataframe converter",
            'python_path': 'd3m.primitives.datasets.NDArrayToDataFramePrimitive',
            'installation': [{
               'type': metadata_base.PrimitiveInstallationType.PIP,
               'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common_primitives'.format(
                   git_commit=d3m_utils.current_git_commit(os.path.dirname(__file__)),
               ),
            }],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
        # Extract the column names so we can add them to the created dataframe
        num_cols = inputs.metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']
        col_names = [inputs.metadata.query((metadata_base.ALL_ELEMENTS, i))['name'] for i in range(0, num_cols)]

        # create a dataframe from the numpy array
        dataframe = container.DataFrame(inputs, columns=col_names)

        # copy the metadata across
        dataframe.metadata = self._update_metadata(inputs.metadata, dataframe, self)

        return base.CallResult(dataframe)

    @classmethod
    def _update_metadata(cls, metadata: metadata_base.DataMetadata, for_value: container.numpy = None, source: typing.Any = None) -> metadata_base.DataMetadata:
        if source is None:
            source = cls

        # set the schema version and structural type
        dataset_metadata = dict(metadata.query(()))
        dataset_metadata.update(
            {
                'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
                'structural_type': container.DataFrame
            },
        )

        # create new metadata initialized with base values
        new_metadata = metadata.clear(dataset_metadata, for_value=for_value, source=source)
        new_metadata = utils.copy_elements_metadata(metadata, (), (), new_metadata, source=source)

        return new_metadata
