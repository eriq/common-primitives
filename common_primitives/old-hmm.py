import time
import os
import math
import random

from typing import NamedTuple, Sequence, Any, List, Dict, Union, Tuple
from d3m_metadata.container.list import List
from d3m_metadata.container.numpy import ndarray
import torch  # type: ignore
import numpy as np  # type: ignore
#  from common_primitives.diagonal_mvn import DiagonalMVN, Hyperparams as MVNHyperparams
from sklearn.metrics import mean_squared_error

from d3m_metadata import hyperparams, params, metadata as metadata_module, utils
from primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from primitive_interfaces.base import ProbabilisticCompositionalityMixin, \
                                      GradientCompositionalityMixin, \
                                      SamplingCompositionalityMixin, \
                                      ContinueFitMixin, \
                                      CallResult, \
                                      Gradients, Scores


from .utils import to_variable, refresh_node, log_mvn_likelihood

# These are just regular Python variables so that we can easily change all types
# at once in the future, if needed. Otherwise, one could simply inline all these.
# 2D array of n_inputs x n_dimensions
Inputs = ndarray
# 1D array of floats of length n_inputs
Outputs = ndarray

# A named tuple for parameters.
# Specifying types for all parameters is important so that one can do end-to-end
# training from outside. For example, some parameters might have gradients so we
# can use those to optimize them end-to-end.
class Hyperparams(hyperparams.Hyperparams):
    transition_prior = hyperparams.Hyperparameter[Union[None, ndarray]](
                            default=None,
                            description='A prior on the transiton matrix KxK that governs the latent state, should be a KxK matrix where each row represent the dirichlet parameters'
                            )
    emission_prior = hyperparams.Hyperparameter[Union[None,ndarray]](
                            default= None, 
                            description='A prior on the emission matrix KxL or a prior on the continuous parameters for y_n'
                            )
    start_prior = hyperparams.Hyperparameter[Union[None,ndarray]](
                            default=None,
                            description='A prior on the initial latent state of the hmm'
                            )


class Params(params.Params):
    #  start_probability: ndarray
    #  emission_probability: ndarray
    #  transition_probability: ndarray
    states: Tuple
    observations: Tuple

class HiddenMarkovModel(ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       GradientCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       SamplingCompositionalityMixin[Inputs, Outputs, Params, Hyperparams],
                       ContinueFitMixin[Inputs, Outputs, Params, Hyperparams],
                       # Base class should be after all mixins.
                       SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):
    """
    Example of a primitive wrapping implementing a linear regression using a high-performance
    PyTorch backend, providing methods from multiple mixins.
    """

    __author__ = "Oxford DARPA D3M Team, William Harvey <willh@robots.ox.ac.uk>"
    metadata = metadata_module.PrimitiveMetadata({
         'id': 'ce649e33-8dc4-4c25-a3bf-9971e94c5468',
         'version': '0.1.0',
         'name': 'Bayesian hmm learning',
         'keywords': ['Bayesian', 'hidden', 'markov', 'model'],
         'source': {
            'name': 'common-primitives',
            'contact' : 'mailto:mteng@robots.ox.ac.uk'
         },
         'installation': [{
            'type': metadata_module.PrimitiveInstallationType.PIP,
            'package_uri': 'git+https://gitlab.com/datadrivendiscovery/common-primitives.git@{git_commit}#egg=common-primitives'.format(git_commit=utils.current_git_commit(os.path.dirname(__file__)))
         }],
         'python_path': 'd3m.primitives.common_primitives.HiddenMarkovModel',
         'algorithm_types': ['LINEAR_REGRESSION'],
         'primitive_family': 'OPERATOR',
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, str] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._transition_prior = hyperparams['transition_prior']
        self._emission_prior = hyperparams['emission_prior']
        self._start_prior = hyperparams['start_prior']

        self._start_prob = None
        self._emit_prob = None
        self._trans_prob = None

        self._is_named_states = False
        self._is_named_observations = False
        self._states = None
        self._observations = None

        self._new_training_data = False
        self_has_supervision = False
        self._training_inputs = None
        self._training_outputs = None

        self._fit_term_temperature = None
        self._has_finished = None
        self._iterations_done = None

    def backward(self, *, gradient_outputs: Gradients[Outputs], fine_tune: bool = False, fine_tune_learning_rate: float = 0.00001,
                 fine_tune_weight_decay: float = 0.00001) -> Tuple[Gradients[Inputs], Scores[Params]]:  # type: ignore
        raise NotImplementedError
        return 0

    def continue_fit(self, *, timeout: float = None, iterations: int = None) -> None:
        return None

    def fit(self, *, timeout: float = None, iterations: int = None, fit_threshold: float, batch_size: int) -> CallResult:
        #  print(self._emit_prob.numpy()[:, 0])
        if self._new_training_data == True:
            #  print("first test is ", first)
            #  self._forward_backward(first)
            for _ in range(iterations):
                obs_0 = self._training_inputs[0]
                alpha, beta, p_x = self._forward_backward(obs_0)
                sum_X_gamma = self._posterior_z_given_x_theta(obs_0, alpha, beta, p_x)
                sum_X_epsilon = self._epsilon_z_to_z_plus_1(obs_0, alpha, beta, p_x)
                #  print(obs_0)
                #  print(sum_X_gamma)
                #  print(self._training_input_masks[0])
                sum_X_gamma_obs = np.array([[np.dot(sum_X_gamma[:, z_i], self._training_input_masks[0][x_i]) \
                        for x_i in range(len(self._observations))] \
                            for z_i in range(len(self._states))])
                idx = 1
                for x in self._training_inputs[1:]:
                    alpha, beta, p_x = self._forward_backward(x)
                    p_z = self._posterior_z_given_x_theta(x, alpha, beta, p_x)
                    
                    sum_X_gamma_obs += np.array([[np.dot(p_z[:, z_i], self._training_input_masks[idx][x_i]) \
                            for x_i in range(len(self._observations))] \
                                for z_i in range(len(self._states))])

                    sum_X_gamma += p_z 
                    p_zi_zj = self._epsilon_z_to_z_plus_1(x, alpha, beta, p_x)
                    sum_X_epsilon += p_zi_zj
                    idx += 1

                #  print("sum of posterior z_t ", sum_X_epsilon)
                #  print("sum of posterior z_i_j ", sum_X_gamma)
                #  print(len(self._training_inputs[0]))
                states = range(len(self._states))
                observations = range(len(self._observations))

                start_i = sum_X_gamma[0]
                if self._start_prior is not None:
                    start_i += np.subtract(self._start_prior, 1)
                
                if self._transition_prior is not None:
                    for i in states:
                        sum_X_epsilon[i] += np.subtract(self._transition_prior[i], 1)

                if self._emission_prior is not None:
                    for i in states:
                        sum_X_gamma_obs[i] += np.subtract(self._emission_prior[i], 1)

                #  print("start_i ", start_i)
                #  print("sum x ep ", sum_X_epsilon)
                #  print(sum_X_gamma_obs)

                start_i_prime = np.array([np.divide(start_i, np.sum(start_i))])

                A_i_prime = np.array([np.divide(sum_X_epsilon[i], \
                                                np.sum(sum_X_gamma[:(len(self._training_inputs[0])) - 1, i]) + np.sum(np.subtract(self._transition_prior[0], 1))) \
                                        for i in range(len(self._states))])

                B_i_prime = np.array([np.divide(sum_X_gamma_obs[i], \
                                                np.sum(np.subtract(self._transition_prior[i], 1)) + np.sum(sum_X_gamma[:, i])) \
                                        for i in range(len(self._states))])
                print("\n\nold start probability is: \n", self._start_prob.numpy())
                print("new start probability is: \n", start_i_prime)
                self._start_prob = torch.Tensor(start_i_prime)

                print("\nold transition probability is: \n", self._trans_prob.numpy())
                print("new transition probability is: \n", A_i_prime)
                self._trans_prob = torch.Tensor(A_i_prime)

                print("\nold emissions probability is: \n", self._emit_prob.numpy())
                print("new emissions probability is: \n", B_i_prime)
                self._emit_prob = torch.Tensor(B_i_prime)

        if self._has_finished == True:
            self._new_training_data = False

        


            
        #  self._forward_backward()
        return CallResult(0, has_finished=self._has_finished, iterations_done=self._iterations_done)
    
    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        return CallResult(np.array[0])

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        #  if d < 1000 switch to analytic form
        #  if inputs.shape[0] < 1000:
        #      self._use_analytic_form = True
        #  if self._has_supervision


        if self._is_named_states is True:
            vf = np.vectorize(self._state_to_int)
            self._training_outputs = vf(outputs)
            #  print("states became: ", self._training_outputs)

        if self._is_named_observations is True:
            vf = np.vectorize(self._observation_to_int)
            self._training_inputs = vf(inputs)

        self._training_input_masks = []
        for seq in self._training_inputs:
            mask = []
            obs = range(len(self._observations))
            for ob in seq:
                m_i = np.array([0 for _ in obs])
                m_i[ob] = 1
                mask.append(m_i)
            self._training_input_masks.append(np.array(mask).transpose())

        self._new_training_data = True
        self._supervised = True if len(inputs) > 0 else False

    def get_params(self) -> Params:
        # return the mean of the posterior if its analytic
        return Params(weights=ndarray(self._weights.data.numpy()), offset=float(self._offset.data.numpy()[0]), noise_variance=float(self._noise_variance.data.numpy()[0]))

    def _state_to_int(self, x):
        return self._states.index(x) 

    def _observation_to_int(self, x):
        return self._observations.index(x) 

    def _int_to_state(self, i):
        return self._states[i]

    def _int_to_observation(self, i):
        return self._observation[i]

    def set_params(self, *, params: Params) -> None:
        self._states = params['states']
        self._observations = params['observations']

        try:
            for elem in self._states:
                int(elem)
        except:
            self._is_named_states = True

        try:
            for elem in self._observations:
                int(elem)
        except:
            self._is_named_observations = True
        #  self._

        K = len(self._states)
        L = len(self._observations)

        self._start_prob = torch.Tensor( np.random.dirichlet(tuple(1 for _ in range(K)), 1) )
        self._start_prior = np.ones(self._start_prob.size())[0]
        #  print(self._start_prior)
        #  print("initial start probability is: ", self._start_prob)

        self._trans_prob = torch.Tensor( np.random.dirichlet(tuple(1 for _ in range(K)), K) )
        self._transition_prior = np.ones(self._trans_prob.size())
        #  print("initial transition probability is: ", self._trans_prob)

        self._emit_prob = torch.Tensor( np.random.dirichlet(tuple(1 for _ in range(L)), K) )
        self._emission_prior = np.ones(self._emit_prob.size())


        #  print("initial emissions probability is: ", self._emit_prob)


        #  self._weights.retain_grad()
        #  self._noise_variance = to_variable(params['noise_variance'], requires_grad=True)
        #  self._offset = to_variable(params['offset'], requires_grad=True)
    def _forward_backward(self, obs_sequence):
        states = range(len(self._states))
        alpha_k = []
        # base case, starting states at t = 0
        start_prob = self._start_prob.numpy().tolist()[0]
        #  print("start prob as list is ", start_prob)
        alpha_k.append([start_prob[i_t0_state] * self._emit_prob[i_t0_state][obs_sequence[0]] for i_t0_state in states])

        for t, x_t in enumerate(obs_sequence[1:]):
            # previous state total probability
            alpha_i = alpha_k[t]
            # total probability of all obs up to t ending in j state

            alpha_j = [sum([alpha_i[i_state] * self._trans_prob[i_state][j_state] * self._emit_prob[j_state][x_t] \
                        for i_state in range(len(self._states))]) \
                            for j_state in range(len(self._states))]
            alpha_k.append(alpha_j)

        # backward part of the algorithm
        backward_obs_sequence = list(reversed(obs_sequence))
        beta_k = []
        # base case, final value
        #  x_t = backward_obs_sequence[0]
        beta_k.append([sum([self._trans_prob[j_t_end_state][end] for end in states]) for j_t_end_state in states])
        #  beta_k.append([self._emit_prob[j_t_end_state][x_t] for j_t_end_state in states])
        #  for t, x_t in enumerate(backward_obs_sequence[1:]):
        beta_j = beta_k[0]
        for t, x_t in enumerate(backward_obs_sequence):
            # next state probability 
            #  beta_j = beta_k[t]
            # total probability of obs leading to i state
            beta_i = [sum([self._emit_prob[j_state][x_t] * self._trans_prob[i_state][j_state] * beta_j[j_state] \
                        for j_state in states]) \
                            for i_state in states]
            beta_k.append(beta_i)
            beta_j = beta_i

        # multiply in start probs for the backward to check that the calculation is the same
        beta_k[-1] = [beta_k[-1][i_t0_state] * start_prob[i_t0_state] for i_t0_state in states]


        p_x_forward = sum(alpha_k[-1][k] for k in states)
        p_x_backward = sum(beta_k[-1][k] for k in states)
        #  print("forward total prob is ", p_x_forward)
        #  print("backward total prob is ", p_x_backward)
        return np.array(alpha_k), np.array(list(reversed(beta_k))[1:]), p_x_forward

    def _posterior_z_given_x_theta(self, obs_sequence, alpha, beta, p_x):
        #  alpha, beta, p_x = self._forward_backward(obs_sequence)
        #  print()
        #  print("\n\n\nalpha is ", np.divide)
        #  print("\n\nbeta is ",)
        p_z_given_x_theta = []
        #  epsilon_z_to_z_plus_1 = []
        for i in range(len(obs_sequence)):
            p_z_given_x_theta.append([alpha[i][z] * beta[i][z] / p_x for z in range(len(self._states))])
        p_z_given_x_theta = np.array(p_z_given_x_theta)

        #  print("\n\nposterior is ", p_z_given_x_theta, "\n\n")
        return p_z_given_x_theta

    def _epsilon_z_to_z_plus_1(self, obs_sequence, alpha, beta, p_x):
        states = range(len(self._states))
        epsilons = []
        for i in range(len(obs_sequence[1:])):
            x_t_plus_1 = obs_sequence[i + 1]
            epsilons.append([[(alpha[i][z_i]*self._trans_prob[z_i][z_j]*self._emit_prob[z_j][x_t_plus_1]*beta[i + 1][z_j]) / p_x for z_j in states] for z_i in states])
            epsilons
        epsilons = np.sum(np.array(epsilons), axis=0)
        #  epsilon_i_j = alpha[i
        
        #  print("\n\nepsilon is ", np.sum(epsilons, axis=0), "\n\n")
        return epsilons


    #  def _posterior_z_(self, obs_sequence):
    #      alpha, beta, p_x = self._forward_backward(obs_sequence)
    #      print("alpha lenght is ", len(alpha))
    #      print("beta length is ", len(beta))
    #      p_z_given_x_theta = []
    #      for i in range(len(obs_sequence)):
    #          p_z_given_x_theta.append([alpha[i][z] * beta[i][z] / p_x for z in range(len(self._states))])
    #
    #      print(p_z_given_x_theta)
    #      return p_z_given_x_theta
        
    def _start_prob_update(self):
        pass
        #  posterior = []
        #  for i in range(len(observations)):
        #      posterior.append({st: fwd[i][st] * bkw[i][st] / p_fwd for st in states})

    def _gamma_t(i, j, t, obs_t, alpha, beta):
        return alpha[t] * self._trans_prob[i][j] * self._emit_prob[j][obs_t] * beta

    def log_likelihoods(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[ndarray]:
        return CallResult(None)

    def log_likelihood(self, *, outputs: Outputs, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[float]:
        result = self.log_likelihoods(outputs=outputs, inputs=inputs, timeout=timeout, iterations=iterations)

        return CallResult(sum(result.value), has_finished=result.has_finished, iterations_done=result.iterations_done)

    def gradient_output(self, *, outputs: Outputs, inputs: Inputs) -> Gradients[Outputs]: # type: ignore
        return np.array([1.0])

    def gradient_params(self, *, outputs: Outputs, inputs: Inputs) -> Scores[Params]:  # type: ignore
        return Params(weights=grad_weights, offset=grad_offset, noise_variance=grad_noise_variance)

    def set_fit_term_temperature(self, *, temperature: float = 0) -> None:
        self._fit_term_temperature = temperature

    def sample(self, *, inputs: Inputs, num_samples: int = 1, timeout: float = None, iterations: int = None) -> Sequence[Outputs]:
        return []

    def get_call_metadata(self) -> CallResult:
        return CallResult(None, has_finished=self._has_finished, iterations_done=self._iterations_done)
