#!/bin/bash -e

# Assumption is that this repository is cloned into "common-primitives" directory
# which is a sibling of "primitives_repo" directory.

PRIMITIVES=(
    d3m.primitives.data.ExtractColumnsBySemanticTypes
    d3m.primitives.data.ExtractColumnsByDtype
    d3m.primitives.data.Concat
    d3m.primitives.data.CastToType
    #d3m.primitives.data.ColumnParser
    #d3m.primitives.data.ConstructPredictions
    #d3m.primitives.datasets.Denormalize
    d3m.primitives.datasets.DatasetToDataFrame
    d3m.primitives.labels.OneHotMaker
    d3m.primitives.datasets.UpdateSemanticTypes
    d3m.primitives.datasets.RemoveColumns
)

for PRIMITIVE in ${PRIMITIVES[@]}; do
  echo $PRIMITIVE
  python -m d3m.index describe -i 4 $PRIMITIVE > primitive.json
  pushd ../primitives_repo
  ./add.py ../common-primitives/primitive.json
  popd
done
